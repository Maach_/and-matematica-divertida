package nunes.chmielewski.andrey.marco.matematicadivertida;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ContagemActivity extends AppCompatActivity {

    private final String titleName = "Contagem";

    Integer[] totalItens = {
           2, 3, 3, 4, 5,
           5, 6, 7, 8, 9
    };

    Integer[] imagens = {
            R.drawable.contagem_02, R.drawable.contagem_03, R.drawable.contagem_03_2, R.drawable.contagem_04, R.drawable.contagem_05,
            R.drawable.contagem_05_2, R.drawable.contagem_06, R.drawable.contagem_07, R.drawable.contagem_08, R.drawable.contagem_09
    };

    Random random;

    ImageView imagemContagem;
    Integer btnRespCorreta;
    Integer respCorreta;
    ArrayList<Integer> valoresGerados;
    Integer totalPerguntas;
    ArrayList<Integer> imagensUtilizadas;
    Integer acertos;
    Button[] botoesResp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contagem);
        setTitle(titleName);

        botoesResp = new Button[] {
                findViewById(R.id.btnResp1),
                findViewById(R.id.btnResp2),
                findViewById(R.id.btnResp3)
        };

        totalPerguntas = 0;
        acertos = 0;
        respCorreta = 0;
        imagensUtilizadas = new ArrayList<>();
        random = new Random(System.currentTimeMillis());
        int valorImagem = random.nextInt( 10);
        int valorBotao = random.nextInt(3);

        imagemContagem = findViewById(R.id.imagemContagem);
        imagemContagem.setBackgroundColor(Color.rgb(255, 255, 255));

        generateNewQuestion(valorImagem, valorBotao);
    }

    public void validaReposta(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String mensagem;

        if (view.getId() == btnRespCorreta) {
            mensagem = "Parabéns";
            builder
                    .setTitle("Correto");
            acertos++;
        } else {
            mensagem = "A resposta correta era: " + respCorreta.toString();
            builder
                    .setTitle("Errado");
        }

        builder.setMessage(mensagem);
        if (totalPerguntas < 5) {
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    generateNewQuestion(random.nextInt(10), random.nextInt(3));
                }
            });
        } else {
            builder
                .setMessage(mensagem + "\nPorcentagem de acertos: " + (100/5 * acertos) + "%")
                .setTitle("Fim de jogo")
                .setPositiveButton("Voltar ao menu", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        }

        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void generateNewQuestion (Integer valorImagem, Integer valorBotao) {
        boolean existente = false;

        for (int i=0; i < totalPerguntas; i++) {
            if (imagensUtilizadas.get(i) == valorImagem)
                existente = true;
        }

        if (!existente) {
            imagemContagem.setImageResource(imagens[valorImagem]);

            botoesResp[valorBotao].setText(totalItens[valorImagem].toString());
            btnRespCorreta = botoesResp[valorBotao].getId();

            valoresGerados = randomizeRespostas(totalItens[valorImagem]);
            if (valorBotao == 0) {
                botoesResp[1].setText(valoresGerados.get(0).toString());
                botoesResp[2].setText(valoresGerados.get(1).toString());
            } else if (valorBotao == 1) {
                botoesResp[0].setText(valoresGerados.get(0).toString());
                botoesResp[2].setText(valoresGerados.get(1).toString());
            } else {
                botoesResp[0].setText(valoresGerados.get(0).toString());
                botoesResp[1].setText(valoresGerados.get(1).toString());
            }

            totalPerguntas++;
            imagensUtilizadas.add(valorImagem);
            respCorreta = totalItens[valorImagem];
            setTitle(titleName + " | " + totalPerguntas + "/5");
        } else {
            generateNewQuestion(random.nextInt( 10), random.nextInt(3));
        }
    }

    public ArrayList<Integer> randomizeRespostas(Integer valorCorreto) {
        ArrayList<Integer> valores = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            if (i != valorCorreto)
                valores.add(i);
        }
        Collections.shuffle(valores);

        return valores;
    }
}
