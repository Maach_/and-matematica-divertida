package nunes.chmielewski.andrey.marco.matematicadivertida;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    public String [] listaMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView lista = findViewById(R.id.listaMenu);

        listaMenu = new String[] {"Contagem", "Aritmética Básica", "Maior Número"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaMenu);
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent tela = null;

                switch (listaMenu[position]) {
                    case "Contagem": tela = new Intent(MainActivity.this, ContagemActivity.class); break;
                    case "Aritmética Básica": tela = new Intent(MainActivity.this, AritmeticaActivity.class); break;
                    case "Maior Número": tela = new Intent(MainActivity.this, MaiorNumeroActivity.class); break;
                }

                if (tela != null)
                    startActivity(tela);
            }
        });
    }
}
