package nunes.chmielewski.andrey.marco.matematicadivertida;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MaiorNumeroActivity extends AppCompatActivity {

    private final String titleName = "Maior Número";

    TextView numeros;
    EditText respostaMaior;
    Button btnResponderMaior;

    Random random;
    int valorResposta;
    int totalPerguntas;
    int acertos;

    ArrayList<String> sequenciasUtilizadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maior_numero);
        setTitle(titleName);

        random = new Random(System.currentTimeMillis());
        valorResposta = 0;
        totalPerguntas = 0;
        acertos = 0;
        sequenciasUtilizadas = new ArrayList<>();

        numeros = findViewById(R.id.numeros);
        respostaMaior = findViewById(R.id.respMaior);
        btnResponderMaior = findViewById(R.id.btnResponderMaior);

        btnResponderMaior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarResposta();
            }
        });

        numeros.setText(generateNumeros());

    }

    public void validarResposta() {
        if (respostaMaior.getText().toString().length() != 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            String mensagem;

            if (Integer.parseInt(respostaMaior.getText().toString()) == valorResposta) {
                mensagem = "Parabéns";
                builder
                        .setTitle("Correto");
                acertos++;
            } else {
                mensagem = "A resposta correta era: " + valorResposta;
                builder
                        .setTitle("Errado");
            }

            builder.setMessage(mensagem);
            if (totalPerguntas < 5) {
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        numeros.setText(generateNumeros());
                    }
                });
            } else {
                builder
                        .setMessage(mensagem + "\nPorcentagem de acertos: " + (100 / 5 * acertos) + "%")
                        .setTitle("Fim de jogo")
                        .setPositiveButton("Voltar ao menu", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
            }

            respostaMaior.setText("");
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(this, "Os campos devem ser preenchidos", Toast.LENGTH_LONG).show();
        }
    }

    public String generateNumeros() {
        ArrayList<Integer> numerosGerados = new ArrayList<>();
        String valorFinal;
        int centena = 0, dezena = 0, unidade = 0;
        int posNumero = 0;


        for (int i=0; i<3; i++)
            numerosGerados.add(random.nextInt(10));

        valorFinal = numerosGerados.get(0).toString() + " " + numerosGerados.get(1).toString() + " " + numerosGerados.get(2).toString();

        for (int i=0; i<numerosGerados.size(); i++){
            if (centena < numerosGerados.get(i) || i == 0) {
                centena = numerosGerados.get(i);
                posNumero = i;
            }
        }
        numerosGerados.remove(posNumero);

        for (int i=0; i<numerosGerados.size(); i++){
            if (dezena < numerosGerados.get(i) || i == 0) {
                dezena = numerosGerados.get(i);
                posNumero = i;
            }
        }
        numerosGerados.remove(posNumero);

        unidade = numerosGerados.get(0);

        valorResposta = (centena*100) + (dezena*10) + unidade;

        for (int i=0; i<sequenciasUtilizadas.size(); i++) {
            if (valorFinal.equals(sequenciasUtilizadas.get(i))) {
                generateNumeros();
                break;
            }
        }

        sequenciasUtilizadas.add(valorFinal);
        totalPerguntas++;
        setTitle(titleName + " | " + totalPerguntas + "/5");
        return valorFinal;
    }
}
