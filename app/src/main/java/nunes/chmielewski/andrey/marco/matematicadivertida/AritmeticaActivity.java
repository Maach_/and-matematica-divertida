package nunes.chmielewski.andrey.marco.matematicadivertida;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class AritmeticaActivity extends AppCompatActivity {

    private final String titleName = "Aritmética Básica";

    TextView operacao;
    EditText resposta;
    Button btnResposta;

    int valorResposta;
    int totalPerguntas;
    int acertos;
    Random random;

    ArrayList<String> operacoesUtilizadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aritmetica);
        setTitle(titleName);

        operacao = findViewById(R.id.operacao);
        resposta = findViewById(R.id.resposta);
        btnResposta = findViewById(R.id.btnResponder);

        totalPerguntas = 0;
        acertos = 0;
        operacoesUtilizadas = new ArrayList<>();
        random = new Random(System.currentTimeMillis());
        operacao.setText(generateOperacao());

        btnResposta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarResposta();
            }
        });
    }

    public void validarResposta() {
        if (resposta.getText().toString().length() != 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            String mensagem;

            if (Integer.parseInt(resposta.getText().toString()) == valorResposta) {
                mensagem = "Parabéns";
                builder
                        .setTitle("Correto");
                acertos++;
            } else {
                mensagem = "A resposta correta era: " + valorResposta;
                builder
                        .setTitle("Errado");
            }

            builder.setMessage(mensagem);
            if (totalPerguntas < 5) {
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        operacao.setText(generateOperacao());
                    }
                });
            } else {
                builder
                        .setMessage(mensagem + "\nPorcentagem de acertos: " + (100 / 5 * acertos) + "%")
                        .setTitle("Fim de jogo")
                        .setPositiveButton("Voltar ao menu", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
            }

            resposta.setText("");
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(this, "Os campos devem ser preenchidos", Toast.LENGTH_LONG).show();
        }
    }

    public String generateOperacao() {
        String operacaoGerada;
        int operando1, operando2, operador;
        operando1 = random.nextInt(10);
        operando2 = random.nextInt(10);
        operador = random.nextInt(2);

        if (operador == 0) {
            operacaoGerada = ((operando1 < operando2 ? operando2 : operando1) + " - " + (operando1 >= operando2 ? operando2 : operando1)).toString();
            valorResposta = (operando1 < operando2 ? operando2 : operando1) - (operando1 >= operando2 ? operando2 : operando1);
        } else {
            operacaoGerada = (operando1 + " + " + operando2).toString();
            valorResposta = operando1 + operando2;
        }

        for (int i=0; i<operacoesUtilizadas.size(); i++) {
            if (operacaoGerada.equals(operacoesUtilizadas.get(i))) {
                generateOperacao();
                break;
            }
        }

        operacoesUtilizadas.add(operacaoGerada);
        totalPerguntas++;
        setTitle(titleName + " | " + totalPerguntas + "/5");
        return operacaoGerada;
    }
}